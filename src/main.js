import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import zhCn from 'element-plus/dist/locale/zh-cn.mjs'
import store from './store/index'
import './mock/index'
//axios文件
import axios from './utils/request.js'

const app = createApp(App)

app.use(ElementPlus)
app.use(ElementPlus, {
  locale: zhCn
})
app.use(router)
app.use(store)
app.mount('#app')
app.config.globalProperties.$axios=axios;  //配置axios的全局引用
