import Mock from 'mockjs'
import { pictureData1, pictureData2 } from './home.js' // 确保路径正确
import { comment } from './likeAndComment' // 确保路径正确
const Random = Mock.Random
Mock.mock('/api/mockGetList', 'get', () => {
  return {
    code: '0',
    data: tableList
  }
})

Mock.mock('/api/add/pictureData1', 'get', () => {
  return {
    code: '0',
    message: 'success',
    data: pictureData1
  }
})
Mock.mock('/api/add/pictureData2', 'get', () => {
  return {
    code: '0',
    message: 'success',
    data: pictureData2
  }
})

let tableList = [] // 用于模拟保存注册成功的用户数据
Mock.mock('/api/register', 'post', (params) => {
  tableList = JSON.parse(localStorage.getItem('registeredUsers')) || []
  const { email, password } = JSON.parse(params.body)
  const existingUser = tableList.find((user) => user.email === email)
  if (existingUser) {
    return {
      code: '-1',
      message: '账号已经被注册'
    }
  }
  const newUser = {
    id: Random.guid(),
    email,
    password
  }

  tableList.push(newUser)
  // 在注册成功后将用户信息保存在本地存储
  localStorage.setItem('registeredUsers', JSON.stringify(tableList))
  return {
    code: '0',
    message: '注册成功'
  }
})

Mock.mock('/api/login', 'post', (params) => {
  const { account, password } = JSON.parse(params.body)
  const registeredUsers =
    JSON.parse(localStorage.getItem('registeredUsers')) || []

  const user = registeredUsers.find(
    (item) => item.account === account && item.password === password
  )
  if (user) {
    // 模拟登录成功
    localStorage.setItem('loggedInUser', JSON.stringify(user))
    return {
      code: '0',
      message: '登录成功',
      data: user
    }
  } else {
    return {
      code: '-1',
      message: '账号或密码错误'
    }
  }
})

// 从本地存储获取表单数据，如果没有则初始化为空数组
let formDataList = JSON.parse(localStorage.getItem('formDataList')) || []
Mock.mock('/api/addFormData', 'post', (params) => {
  const newFormData = JSON.parse(params.body)

  const id = Random.guid()
  formDataList.push({
    id,
    ...newFormData
  })
  // 更新本地存储的数据
  localStorage.setItem('formDataList', JSON.stringify(formDataList))
  return {
    code: '0',
    message: '表单数据添加成功',
    data: formDataList
  }
})

// Mock.js 模拟获取表单列表的接口
Mock.mock('/api/getFormList', 'get', () => {
  return {
    code: '0',
    message: '获取表单列表成功',
    data: formDataList
  }
})
// Mock.js 模拟修改表单数据的接口
Mock.mock('/api/updateFormData', 'post', (params) => {
  const updatedForm = JSON.parse(params.body)

  // 根据传入的表单数据中的 ID 查找对应的表单并进行更新
  formDataList = formDataList.map((formData) => {
    if (formData.id === updatedForm.id) {
      return {
        ...formData,
        departureDate: updatedForm.departureDate,
        destination: updatedForm.destination,
        transportation: updatedForm.transportation
      }
    }
    return formData
  })

  // 更新本地存储的数据
  localStorage.setItem('formDataList', JSON.stringify(formDataList))
  return {
    code: '0',
    message: '表单数据更新成功',
    data: formDataList
  }
})

// Mock.js 模拟删除表单数据的接口
Mock.mock('/api/deleteFormData', 'post', (params) => {
  const { id } = JSON.parse(params.body)

  // 根据传入的 ID 进行删除操作
  formDataList = formDataList.filter((formData) => formData.id !== id)

  // 更新本地存储的数据
  localStorage.setItem('formDataList', JSON.stringify(formDataList))

  return {
    code: '0',
    message: '表单数据删除成功',
    data: formDataList
  }
})

// 获取所有评论
const getComments = () => {
  const storedComments = localStorage.getItem('comments')
  return storedComments ? JSON.parse(storedComments) : []
}

// 保存所有评论到 localStorage
const saveComments = (data) => {
  localStorage.setItem('comments', JSON.stringify(data))
}
if (!localStorage.getItem('comments')) {
  localStorage.setItem('comments', JSON.stringify(comment))
}
// Mock API - 获取所有评论
Mock.mock('/api/comments', 'get', () => {
  return getComments()
})

// // Mock API - 添加评论
// Mock.mock('/api/addComment', 'post', (options) => {
//   const newComment = JSON.parse(options.body)
//   const comments = getComments()
//   comments.push(newComment)
//   saveComments(comments)
//   return { message: 'Comment added successfully' }
// })

// Mock API - 添加回复
Mock.mock('/api/addReply', 'post', (options) => {
  const { commentId, reply } = JSON.parse(options.body)
  // console.log(commentId, reply)
  const comments = getComments()
  const comment = comments.find((c) => c.id === commentId)
  console.log(comment)
  if (comment) {
    comment.reply.push(reply)
    saveComments(comments)
    return { message: 'Reply added successfully' }
  }
  return { message: 'Comment not found' }
})

// Mock API - 点赞评论
Mock.mock('/api/likeComment', 'post', (options) => {
  const { commentId } = JSON.parse(options.body)
  const comments = getComments()
  const comment = comments.find((c) => c.id === commentId)
  if (comment) {
    comment.likeNum++
    saveComments(comments)
    return { message: 'Comment liked successfully' }
  }
  return { message: 'Comment not found' }
})
