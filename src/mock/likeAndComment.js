//模拟评论数据
const comment = [
  {
    id: 'comment0001', //主键id
    date: '2018-07-05 08:30', //评论时间
    ownerId: 'talents100020', //文章的id
    Image: 'carousel-1.jpg',
    fromId: 'errhefe232213', //评论者id
    fromName: '犀利的评论家', //评论者昵称
    fromAvatar:
      'http://ww4.sinaimg.cn/bmiddle/006DLFVFgy1ft0j2pddjuj30v90uvagf.jpg', //评论者头像
    likeNum: 3, //点赞人数
    content:
      '第一次出国是 日本 ，好歹是满街老乡和汉字。而第二次出国就 格鲁吉亚 ，高难度啊！他们都说俄语和 格鲁吉亚 语，听说全国人民跟我一样英 文成 绩不太好。那见面说什么呀，翻译器灵不灵呀？……这让我充满了忐忑和不安👹！在这里，我还将要度过人生第4⃣0⃣个生日，想到这又充满了兴奋和刺激👻👻👻', //评论内容
    reply: [
      //回复，或子评论
      {
        id: '34523244545', //主键id
        commentId: 'comment0001', //父评论id，即父亲的id
        fromId: 'observer223432', //评论者id
        fromName: '夕阳红', //评论者昵称
        fromAvatar:
          'https://wx4.sinaimg.cn/mw690/69e273f8gy1ft1541dmb7j215o0qv7wh.jpg', //评论者头像
        toId: 'errhefe232213', //被评论者id
        toName: '犀利的评论家', //被评论者昵称
        toAvatar:
          'http://ww4.sinaimg.cn/bmiddle/006DLFVFgy1ft0j2pddjuj30v90uvagf.jpg', //被评论者头像
        content: '赞同，很靠谱，水平很高', //评论内容
        date: '2018-07-05 08:35' //评论时间
      },
      {
        id: '34523244545',
        commentId: 'comment0001',
        fromId: 'observer567422',
        fromName: '清晨一缕阳光',
        fromAvatar:
          'http://imgsrc.baidu.com/imgad/pic/item/c2fdfc039245d688fcba1b80aec27d1ed21b245d.jpg',
        toId: 'observer223432',
        toName: '夕阳红',
        toAvatar:
          'https://wx4.sinaimg.cn/mw690/69e273f8gy1ft1541dmb7j215o0qv7wh.jpg',
        content: '大神一个！',
        date: '2018-07-05 08:50'
      }
    ]
  },
  {
    id: 'comment0002',
    date: '2018-07-05 08:30',
    Image: 'carousel-2.jpg.png',
    ownerId: 'talents100020',
    fromId: 'errhefe232213',
    fromName: '毒蛇郭德纲',
    fromAvatar:
      'http://ww1.sinaimg.cn/bmiddle/006DLFVFgy1ft0j2q2p8pj30v90uzmzz.jpg',
    likeNum: 0,
    content:
      '我希望，年迈时能够住在一个小农场，有马有狗，养鹰种茶花。到时候，老朋友相濡以沫住在一起，读书种地，酿酒喝 普洱 茶。我们齐心合力盖房子，每个窗户都是不同颜色的。谁的屋顶漏雨，我们就一起去修补它。我们敲起手鼓咚咚哒，唱起老歌跳舞围着篝火哦。如果谁死了，我们就弹吉他欢送他。这个世界是不是你想要的，为什么那么纠结于它。简单的生活呀，触手可及吗？不如接下来，咱们一起出发。--大冰《他们最幸福》',
    reply: [
      {
        id: '34523244545',
        commentId: 'comment0002',
        fromId: 'observer567422',
        fromName: '清晨一缕阳光',
        fromAvatar:
          'http://imgsrc.baidu.com/imgad/pic/item/c2fdfc039245d688fcba1b80aec27d1ed21b245d.jpg',
        toId: 'errhefe232213',
        toName: '毒蛇郭德纲',
        toAvatar:
          'https://wx4.sinaimg.cn/mw690/69e273f8gy1ft1541dmb7j215o0qv7wh.jpg',
        content: '大神一个！',
        date: '2018-07-05 08:50'
      }
    ]
  }
]

export { comment }
