import { reactive } from 'vue' // 如果你正在使用 Vue 3

export const pictureData1 = reactive([
  {
    title: '＜奇境南美巴西阿根廷智利１０天＞',
    image: '558 (2).jpg',
    price: '15600'
  },

  {
    title: '＜欧洲美食之旅法国瑞士意大利９晚１０天＞',
    image: '155 (1).jpg',
    price: '13500'
  },
  {
    title: '＜东南亚游轮体验新加坡马来西亚泰国５晚６天＞',
    image: '155 (2).jpg',
    price: '9800'
  },
  {
    title: '＜古埃及探秘之旅开罗卢克索亚斯旺４晚５天＞',
    image: '155 (3).jpg',
    price: '9200'
  }
])

export const pictureData2 = reactive([
  {
    headline: '春节出游',
    headline1: '春节出游2024全家人的海外新年',
    image: '558 (1).jpg',
    subContent: [
      {
        title: '梦幻巴厘岛之旅',
        image: '197 (24).jpg',
        price: 6999,
        departure: '北京出发'
      },
      {
        title: '意大利浪漫之旅',
        image: '197 (25).jpg',

        price: 8999,
        departure: '上海出发'
      },
      {
        title: '东京迪士尼乐园奇幻之旅',
        image: '197 (26).jpg',

        price: 7999,
        departure: '广州出发'
      },
      {
        title: '马尔代夫沙滩度假',
        image: '197 (27).jpg',

        price: 12999,
        departure: '深圳出发'
      },
      {
        title: '希腊神话之旅',
        image: '197 (28).jpg',

        price: 9999,
        departure: '北京出发'
      },
      {
        title: '巴西狂欢节体验',
        image: '197 (29).jpg',

        price: 10999,
        departure: '上海出发'
      }
    ]
  },
  {
    headline: '春节出游',
    headline1: '春节出游2024全家人的海外新年',
    image: '558 (2).jpg',
    subContent: [
      {
        title: '梦幻巴厘岛之旅',
        image: '197 (1).jpg',
        price: 6999,
        departure: '北京出发'
      },
      {
        title: '意大利浪漫之旅',
        image: '197 (2).jpg',
        price: 8999,
        departure: '上海出发'
      },
      {
        title: '东京迪士尼乐园奇幻之旅',
        image: '197 (3).jpg',
        price: 7999,
        departure: '广州出发'
      },
      {
        title: '马尔代夫沙滩度假',
        image: '197 (4).jpg',
        price: 12999,
        departure: '深圳出发'
      },
      {
        title: '希腊神话之旅',
        image: '197 (5).jpg',
        price: 9999,
        departure: '北京出发'
      },
      {
        title: '巴西狂欢节体验',
        image: '197 (6).jpg',
        price: 10999,
        departure: '上海出发'
      }
    ]
  },
  {
    headline: '春节出游',
    headline1: '春节出游2024全家人的海外新年',
    image: '558 (3).jpg',
    subContent: [
      {
        title: '梦幻巴厘岛之旅',
        image: '197 (6).jpg',
        price: 6999,
        departure: '北京出发'
      },
      {
        title: '意大利浪漫之旅',
        image: '197 (7).jpg',

        price: 8999,
        departure: '上海出发'
      },
      {
        title: '东京迪士尼乐园奇幻之旅',
        image: '197 (8).jpg',

        price: 7999,
        departure: '广州出发'
      },
      {
        title: '马尔代夫沙滩度假',
        image: '197 (9).jpg',

        price: 12999,
        departure: '深圳出发'
      },
      {
        title: '希腊神话之旅',
        image: '197 (10).jpg',

        price: 9999,
        departure: '北京出发'
      },
      {
        title: '巴西狂欢节体验',
        image: '197 (11).jpg',

        price: 10999,
        departure: '上海出发'
      }
    ]
  },
  {
    headline: '春节出游',
    headline1: '春节出游2024全家人的海外新年',
    image: '558 (4).jpg',
    subContent: [
      {
        title: '梦幻巴厘岛之旅',
        image: '197 (12).jpg',

        price: 6999,
        departure: '北京出发'
      },
      {
        title: '意大利浪漫之旅',
        image: '197 (13).jpg',

        price: 8999,
        departure: '上海出发'
      },
      {
        title: '东京迪士尼乐园奇幻之旅',
        image: '197 (14).jpg',

        price: 7999,
        departure: '广州出发'
      },
      {
        title: '马尔代夫沙滩度假',
        image: '197 (15).jpg',

        price: 12999,
        departure: '深圳出发'
      },
      {
        title: '希腊神话之旅',
        image: '197 (16).jpg',

        price: 9999,
        departure: '北京出发'
      },
      {
        title: '巴西狂欢节体验',
        image: '197 (17).jpg',

        price: 10999,
        departure: '上海出发'
      }
    ]
  },
  {
    headline: '春节出游',
    headline1: '春节出游2024全家人的海外新年',
    image: '558 (5).jpg',
    subContent: [
      {
        title: '梦幻巴厘岛之旅',
        image: '197 (18).jpg',

        price: 6999,
        departure: '北京出发'
      },
      {
        title: '意大利浪漫之旅',
        image: '197 (19).jpg',

        price: 8999,
        departure: '上海出发'
      },
      {
        title: '东京迪士尼乐园奇幻之旅',
        image: '197 (20).jpg',

        price: 7999,
        departure: '广州出发'
      },
      {
        title: '马尔代夫沙滩度假',
        image: '197 (21).jpg',

        price: 12999,
        departure: '深圳出发'
      },
      {
        title: '希腊神话之旅',
        image: '197 (22).jpg',

        price: 9999,
        departure: '北京出发'
      },
      {
        title: '巴西狂欢节体验',
        image: '197 (23).jpg',
        price: 10999,
        departure: '上海出发'
      }
    ]
  }
])
