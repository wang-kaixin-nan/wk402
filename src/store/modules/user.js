import { defineStore } from 'pinia'

export const userStore = defineStore('userInfo', {
  state: () => {
    return {
      userID: '10001',
      userName: '某同学'
    }
  },
  getters: {
    fullName: (state) => {
      return `${state.userName}(${state.userID})`
    }
  },
  // action 支持 async/await 的语法，可以自行添加使用
  // action 里的方法相互之间调用，直接用 this 访问即可
  actions: {
    updateUserName(name) {
      this.userName = name
    }
  }
})
