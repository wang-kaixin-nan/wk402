import { createRouter, createWebHistory } from 'vue-router'
import { defineAsyncComponent } from 'vue'

const router = createRouter({
  // history: createWebHashHistory(),  // hash 模式
  history: createWebHistory(), // history 模式
  routes: [
    {
      path: '/',
      redirect: '/home',
      meta: {
        title: '首页',
        hideNavigation: true
      }
    },
    {
      path: '/home',
      name: 'home',

      component: defineAsyncComponent(() => import(`../views/Home`)),
      meta: {
        title: '首页',
        hideNavigation: true
      }
    },

    {
      path: '/tavern',
      name: 'tavern',
      component: defineAsyncComponent(() => import(`../views/tavern`)),
      meta: {
        title: '酒店',
        hideNavigation: true
      }
    },
    {
      path: '/search',
      name: 'search',
      component: defineAsyncComponent(() => import(`../views/tavern/search.vue`)),
      meta: {
        title: '搜索',
        hideNavigation: false
      }
    },
    {
      path: '/hotel',
      name: 'hotel',
      component: defineAsyncComponent(() => import(`../views/tavern/hotel.vue`)),
      meta: {
        title: '酒店',
        hideNavigation: false
      }
    },
    {
      path: '/login',
      name: 'login',
      component: defineAsyncComponent(() => import(`../views/Login`)),
      meta: {
        title: '登陆',
        hideNavigation: false
      }
    },
    {
      path: '/register',
      name: 'register',
      component: defineAsyncComponent(() => import(`../views/register`)),
      meta: {
        title: '注册',
        hideNavigation: false
      }
    },
    {
      path: '/travelChoice',
      name: 'travelChoice',
      component: defineAsyncComponent(() => import(`../views/travelChoice`)),
      meta: {
        title: '出行选择',
        hideNavigation: true
      }
    },
    {
      path: '/tourist-products',
      name: 'tourist-products',
      component: defineAsyncComponent(() => import(`../views/tourist`)),
      meta: {
        title: '旅游路线',
        hideNavigation: true
      }
    },
    {
      path: '/delicacies',
      name: 'delicacies',
      component: defineAsyncComponent(() => import(`../views/delicacies`)),
      meta: {
        title: '景区.美食.住宿',
        hideNavigation: true
      }
    },

    {
      path: '/*',
      redirect: '/'
    }
  ]
})

// 全局路由守卫
router.beforeEach((to, from, next) => {
  const loggedInUser = localStorage.getItem('loggedInUser')

  if (loggedInUser && (to.name === 'login' || to.name === 'register')) {
    next({ name: 'home' })
  } else {
    next()
  }
})

export default router
