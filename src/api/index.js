import http from '../utils/request'
const userService = {
  //用户列表
  findAll(user) {
    return http({
      url: `/api/login`,
      method: 'post',
      data: user
    })
  },
  //添加用户
  addUser(user) {
    return http({
      url: `/api/register`,
      method: 'post',
      data: user
    })
  },
  // 首页数据1
  homePageData1() {
    return http({
      url: `/api/add/pictureData1`,
      method: 'get'
    })
  },
  // 首页数据1
  homePageData2() {
    return http({
      url: `/api/add/pictureData2`,
      method: 'get'
    })
  },
  getFormList() {
    return http({
      url: `/api/getFormList`,
      method: 'get'
    })
  },
  addFormData(user) {
    return http({
      url: '/api/addFormData',
      method: 'post',
      data: user
    })
  },
  updateFormData(user) {
    return http({
      url: '/api/updateFormData',
      method: 'post',
      data: user
    })
  },
  deleteFormData(user) {
    return http({
      url: '/api/deleteFormData',
      method: 'post',
      data: user
    })
  },
  comments() {
    return http({
      url: '/api/comments',
      method: 'get'
    })
  },
  addReply(user) {
    return http({
      url: '/api/addReply',
      method: 'post',
      data: user
    })
  },
  likeComment(user) {
    return http({
      url: '/api/likeComment',
      method: 'post',
      data: user
    })
  }
}
export default userService
